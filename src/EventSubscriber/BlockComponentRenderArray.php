<?php

namespace Drupal\layout_builder_ipe\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\LayoutEntityHelperTrait;
use Drupal\layout_builder_ipe\LayoutBuilder\LayoutBuilderUi;
use Drupal\layout_builder_ipe\LayoutBuilderIpeService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Listen to the block component render event that emitted by layout builder.
 */
class BlockComponentRenderArray implements EventSubscriberInterface {

  use LayoutEntityHelperTrait;

  /**
   * The layout builder IPE service.
   *
   * @var \Drupal\layout_builder_ipe\LayoutBuilderIpeService
   */
  protected $layoutBuilderIpe;

  /**
   * The layout builder UI service.
   *
   * @var \Drupal\layout_builder_ipe\LayoutBuilder\LayoutBuilderUi
   */
  protected $layoutBuilderUi;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs the BlockComponentRenderArray object.
   *
   * @param \Drupal\layout_builder_ipe\LayoutBuilderIpeService $layout_builder_ipe
   *   The IPE service.
   * @param \Drupal\layout_builder_ipe\LayoutBuilder\LayoutBuilderUi $layout_builder_ipe_ui
   *   The IPE UI additions.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(LayoutBuilderIpeService $layout_builder_ipe, LayoutBuilderUi $layout_builder_ipe_ui, RendererInterface $renderer, ConfigFactoryInterface $config_factory) {
    $this->layoutBuilderIpe = $layout_builder_ipe;
    $this->layoutBuilderUi = $layout_builder_ipe_ui;
    $this->renderer = $renderer;
    $this->config = $config_factory->get('layout_builder_ipe.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    // This must be lower than the value in
    // \Drupal\layout_builder\EventSubscriber\BlockComponentRenderArray.
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = [
      'onBuildRender',
      50,
    ];
    return $events;
  }

  /**
   * Act on render arrays for block plugins add additional "Add block" buttons.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    $component = $event->getComponent();
    $contexts = $event->getContexts();

    $view_mode = 'full';
    if (array_key_exists('view_mode', $contexts)) {
      $view_mode = $contexts['view_mode']?->getContextValue() ?? 'full';
    }
    $section_storage = $this->layoutBuilderIpe->getSectionStorage($view_mode);
    if (!$section_storage) {
      return;
    }

    $entity = $this->layoutBuilderIpe->getEntityFromSectionStorage($section_storage);
    if (!$entity || !$this->layoutBuilderIpe->ipeEnabled($entity, $view_mode)) {
      return;
    }

    $sections = $section_storage->getSections();
    $delta = NULL;

    foreach ($sections as $section_delta => $section) {
      $section_components = $section->getComponentsByRegion($component->getRegion());
      if (array_key_exists($component->getUuid(), $section_components)) {
        $delta = $section_delta;
        break;
      }
    }

    $build = $event->getBuild();
    if ($delta !== NULL && $section_storage && $event->inPreview() && $this->config->get('enhance_ui')) {
      $button = $this->layoutBuilderUi->createAddButton($component, $section_storage, $delta, LayoutBuilderUi::BEFORE);
      if (!empty($button) && is_array($button)) {
        $build['content']['#prefix'] = $this->renderer->render($button);
        $event->setBuild($build);
      }
      $button = $this->layoutBuilderUi->createAddButton($component, $section_storage, $delta, LayoutBuilderUi::AFTER);
      if (!empty($button) && is_array($button)) {
        $build['content']['#suffix'] = $this->renderer->render($button);
        $event->setBuild($build);
      }
    }
  }

}
