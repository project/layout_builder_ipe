<?php

namespace Drupal\layout_builder_ipe;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Config service for IPE.
 */
class LayoutBuilderIpeConfig {

  /**
   * The layout builder ipe config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Public constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('layout_builder_ipe.settings');
  }

  /**
   * Get e config value for the given key.
   *
   * @param string $key
   *   The config key.
   *
   * @return mixed
   *   The config value.
   */
  public function get($key) {
    return $this->config->get($key);
  }

  /**
   * Get cache tags associated with the ipe config.
   *
   * @return string[]
   *   A set of cache tags.
   */
  public function getCacheTags() {
    return $this->config->getCacheTags();
  }

}
