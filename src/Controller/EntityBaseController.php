<?php

namespace Drupal\layout_builder_ipe\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Defines a base controller for content entities.
 *
 * @internal
 *   Controller classes are internal.
 */
abstract class EntityBaseController extends BaseController {

  /**
   * Build an ajax response.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response containing the layout builder form.
   */
  protected function buildAjaxResponse(SectionStorageInterface $section_storage) {
    $layout_builder_layout = NULL;
    $layout_builder_form = NULL;
    $layout_builder_message = NULL;

    $entity = $this->layoutBuilderIpe->getEntityFromSectionStorage($section_storage);

    // Get the layout builder form.
    $form_class = $entity->getEntityType()->getFormClass('layout_builder');
    if ($entity && $form_class) {
      $additional_form_state = [
        'build_info' => [
          'args' => [
            'section_storage' => $section_storage,
          ],
        ],
      ];
      $layout_builder_form = $this->entityFormBuilder()->getForm($entity, 'layout_builder', $additional_form_state);
      $layout_builder_layout = $layout_builder_form['layout_builder__layout'];
      unset($layout_builder_form['layout_builder__layout']);
      unset($layout_builder_form['layout_builder_message']);

      // Update action urls in the form, in case this is embedded by external
      // code and not as part of the default route handling. In this case there
      // can be hard to understand issues with form actions not working
      // correctly.
      $this->setFormActionUrl($layout_builder_form, $section_storage, $entity);
    }

    return $this->ajaxResponse([
      $layout_builder_layout,
      $layout_builder_form,
      $layout_builder_message,
    ]);
  }

  /**
   * Get the route name for the current controller.
   *
   * @return string
   *   The route name.
   */
  abstract protected function getRouteName();

  /**
   * Set the form action url to match the route of the called controller.
   *
   * Also update urls in any ajax enabled action button.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The main entity for this request.
   */
  private function setFormActionUrl(array &$form, SectionStorageInterface $section_storage, EntityInterface $entity) {
    $url = Url::fromRoute($this->getRouteName(), [
      'section_storage_type' => $section_storage->getStorageType(),
      'section_storage' => $section_storage->getStorageId(),
      'entity_type' => $entity->getEntityTypeId(),
      'entity' => $entity->id(),
    ]);

    // Get the query parameters for modification.
    $query = $url->getOption('query');

    // Add a destination argument if available.
    $redirect_uri = $this->getRedirectUri();
    if ($redirect_uri) {
      $query['destination'] = $redirect_uri;
      $url->setOption('query', $query);
    }
    $form['#action'] = $url->toString();

    // Mark as ajax form request.
    $query[FormBuilderInterface::AJAX_FORM_REQUEST] = TRUE;
    $url->setOption('query', $query);

    foreach (Element::children($form['actions']) as $key) {
      $element = &$form['actions'][$key] ?? NULL;
      if (!$element || empty($element['#ajax'])) {
        continue;
      }
      $element['#ajax']['url'] = $url->toString();
      $element['#attached']['drupalSettings']['ajax'][$element['#id']]['url'] = $url->toString();
      $element['#attached']['drupalSettings']['ajaxTrustedUrl'][$url->toString()] = TRUE;
    }
  }

}
