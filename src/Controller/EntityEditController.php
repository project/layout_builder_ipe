<?php

namespace Drupal\layout_builder_ipe\Controller;

use Drupal\Core\Url;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Defines an edit controller for content entities.
 *
 * @internal
 *   Controller classes are internal.
 */
class EntityEditController extends EntityBaseController {

  /**
   * Check access to the Layout Builder IPE frontend.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   An access result object.
   */
  public function access(SectionStorageInterface $section_storage) {
    return $this->layoutBuilderIpe->access($section_storage);
  }

  /**
   * Get the edit part of a layout builder page.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response containing the layout builder form.
   */
  public function edit(SectionStorageInterface $section_storage) {
    $destination = $this->requestStack->getCurrentRequest()->query->get('destination');
    if ($destination && Url::fromUserInput($destination)) {
      $this->setRedirectUri($destination);
    }
    return $this->buildAjaxResponse($section_storage);
  }

  /**
   * {@inheritdoc}
   */
  protected function getRouteName() {
    return 'layout_builder_ipe.entity.edit';
  }

}
