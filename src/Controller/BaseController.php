<?php

namespace Drupal\layout_builder_ipe\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base controller to provide the Layout Builder admin UI.
 *
 * @internal
 *   Controller classes are internal.
 */
abstract class BaseController extends ControllerBase {

  /**
   * The layout builder ipe service.
   *
   * @var \Drupal\layout_builder_ipe\LayoutBuilderIpeService
   */
  protected $layoutBuilderIpe;

  /**
   * Layout tempstore repository.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected $layoutTempstoreRepository;

  /**
   * Tempstore Factory.
   *
   * @var \Drupal\Core\TempStore\SharedTempStoreFactory
   */
  protected $tempstore;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * A uri to be used for redirects.
   *
   * @var string
   */
  protected $redirectUri;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->layoutBuilderIpe = $container->get('layout_builder_ipe');
    $instance->layoutTempstoreRepository = $container->get('layout_builder.tempstore_repository');
    $instance->tempstore = $container->get('tempstore.shared');
    $instance->requestStack = $container->get('request_stack');
    return $instance;
  }

  /**
   * Build an ajax response for IPE controllers.
   *
   * @param array|string $content
   *   The content to return.
   * @param bool $editing
   *   Whether IPE is currently in edit more or not.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  protected function ajaxResponse($content, $editing = TRUE) {
    $response = new AjaxResponse();
    if (!empty($content)) {
      // Support the new gin toolbar.
      $libraries = [
        'gin/sidebar',
      ];
      if ($this->moduleHandler()->moduleExists('gin_lb')) {
        /** @var \Drupal\gin_lb\HookHandler\PageAttachments $instance */
        $instance = \Drupal::service('class_resolver')->getInstanceFromDefinition('\Drupal\gin_lb\HookHandler\PageAttachments');
        $attachments = [
          '#attached' => ['library' => []],
        ];
        $instance->attachments($attachments);
        $libraries = array_merge($libraries, $attachments['#attached']['library']);

      }
      $toolbar = [
        '#type' => 'toolbar',
        '#access' => $this->currentUser()->hasPermission('access toolbar'),
        '#cache' => [
          'keys' => ['toolbar'],
          'contexts' => ['user.permissions'],
        ],
        '#attached' => [
          'library' => $libraries,
        ],
      ];

      $response->addCommand(new ReplaceCommand('#gin-toolbar-wrapper', $toolbar));
      $response->addCommand(new ReplaceCommand('#layout-builder-ipe-wrapper', [
        '#type' => 'container',
        '#prefix' => '<div id="layout-builder-ipe-wrapper" class="' . ($editing ? 'edit-layout' : '') . '">',
        '#suffix' => '</div>',
      ] + [$content]));
    }
    return $response;
  }

  /**
   * Set the redirect uri.
   *
   * @param string $uri
   *   The uri to redirect to.
   */
  protected function setRedirectUri($uri) {
    $this->redirectUri = $uri;
  }

  /**
   * Get the redirect uri.
   *
   * @return string
   *   The uri to redirect to.
   */
  protected function getRedirectUri() {
    if (!$this->redirectUri) {
      // If no redirect has been set yet, we try to get the destination
      // parameter from the current request.
      $destination = $this->requestStack->getCurrentRequest()->query->get('destination');
      if ($destination && Url::fromUserInput($destination)) {
        $this->setRedirectUri($destination);
      }
    }
    return $this->redirectUri;
  }

}
