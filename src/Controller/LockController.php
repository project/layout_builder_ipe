<?php

namespace Drupal\layout_builder_ipe\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a lock controller to provide logic around lock-breaking.
 *
 * @internal
 *   Controller classes are internal.
 */
class LockController extends BaseController {

  /**
   * The current request.
   *
   * @var \Drupal\layout_builder_ipe\LayoutBuilderIpeLock
   */
  protected $layoutBuilderLock;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->layoutBuilderLock = $container->get('layout_builder_ipe.lock');
    return $instance;
  }

  /**
   * Check if a lock on the given section storage can be broken.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   An access result object.
   */
  public function access(SectionStorageInterface $section_storage) {
    if (!$this->currentUser()->hasPermission('layout builder ipe break locks')) {
      return AccessResult::forbidden();
    }
    return $this->layoutBuilderIpe->access($section_storage, NULL, FALSE);
  }

  /**
   * Break a lock on a layout builder page.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function breakLock(SectionStorageInterface $section_storage) {
    // Break the lock.
    $this->layoutTempstoreRepository->delete($section_storage);
    // Click the customize link again and close the dialog.
    $ajax_response = new AjaxResponse();
    $ajax_response->addCommand(new InvokeCommand('.layout-builder-ipe--link-customize', 'click'));
    $ajax_response->addCommand(new CloseModalDialogCommand(TRUE));
    return $ajax_response;
  }

}
