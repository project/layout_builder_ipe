<?php

namespace Drupal\layout_builder_ipe\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an override controller for content entities.
 *
 * @internal
 *   Controller classes are internal.
 */
class EntityOverrideController extends EntityBaseController {

  /**
   * Check access to the Layout Builder IPE frontend.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   An access result object.
   */
  public function access(EntityInterface $entity) {
    $section_storage = $this->layoutBuilderIpe->getSectionStorageForEntity($entity);
    if ($section_storage === NULL) {
      return AccessResult::forbidden();
    }
    return $this->layoutBuilderIpe->access($section_storage, $entity);
  }

  /**
   * Get the edit part of a layout builder page.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @return string
   *   The title for the layout page.
   */
  public function override(EntityInterface $entity) {
    $section_storage = $this->layoutBuilderIpe->getSectionStorage();
    if (!$section_storage) {
      // @todo raise exception
      return NULL;
    }
    return $this->buildAjaxResponse($section_storage);
  }

  /**
   * {@inheritdoc}
   */
  protected function getRouteName() {
    return 'layout_builder_ipe.entity.override';
  }

}
