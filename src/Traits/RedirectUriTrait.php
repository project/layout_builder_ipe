<?php

namespace Drupal\layout_builder_ipe\Traits;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Helper trait for section storages in forms.
 */
trait RedirectUriTrait {

  /**
   * Get the current redirect uri..
   *
   * @return string|null
   *   The redirect uri.
   */
  public static function getRedirectUri(FormStateInterface $form_state) {
    $redirect_uri = $form_state->getBuildInfo()['redirect_uri'] ?? NULL;
    if ($redirect_uri) {
      return $redirect_uri;
    }
    $request = \Drupal::request();
    $destination = $request->query->get('destination');
    if ($destination && Url::fromUserInput($destination)) {
      return $destination;
    }

    return NULL;
  }

}
