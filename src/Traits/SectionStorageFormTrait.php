<?php

namespace Drupal\layout_builder_ipe\Traits;

use Drupal\Core\Form\ConfirmFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Form\OverridesEntityForm;

/**
 * Helper trait for section storages in forms.
 */
trait SectionStorageFormTrait {

  /**
   * Get the section storage for the given form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\layout_builder\SectionStorageInterface|null
   *   The section storage.
   */
  public static function getSectionStorageFromFormState(FormStateInterface $form_state) {
    $form_object = $form_state->getFormObject();
    if ($form_object instanceof ConfirmFormInterface) {
      return reset($form_state->getBuildInfo()['args']);
    }
    elseif ($form_object instanceof OverridesEntityForm) {
      return $form_object->getSectionStorage();
    }
  }

}
