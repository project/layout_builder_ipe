<?php

namespace Drupal\layout_builder_ipe;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for Layout Builder overrides.
 *
 * @see \Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage::access()
 *
 * @internal
 *   Dynamic permission callbacks are internal.
 */
class LayoutBuilderIpePermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * LayoutBuilderOverridesPermissions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The bundle info service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $bundle_info, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfo = $bundle_info;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('module_handler')
    );
  }

  /**
   * Returns an array of permissions.
   *
   * @return string[][]
   *   An array whose keys are permission names and whose corresponding values
   *   are defined in \Drupal\user\PermissionHandlerInterface::getPermissions().
   */
  public function permissions() {
    $permissions = [];
    $args = [];

    $permissions['administer layout builder ipe'] = [
      'title' => $this->t('Administer Layout Builder IPE'),
    ];
    $permissions['layout builder ipe break locks'] = [
      'title' => $this->t('Break locks on Layout Builder IPE'),
    ];

    /** @var \Drupal\layout_builder\Entity\LayoutEntityDisplayInterface[] $entity_displays */
    $entity_displays = $this->entityTypeManager->getStorage('entity_view_display')->loadByProperties(['third_party_settings.layout_builder.allow_custom' => TRUE]);
    foreach ($entity_displays as $entity_display) {
      $entity_type_id = $entity_display->getTargetEntityTypeId();
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      $bundle = $entity_display->getTargetBundle();
      $args = [
        '%entity_type' => $entity_type->getCollectionLabel(),
        '@entity_type_singular' => $entity_type->getSingularLabel(),
        '@entity_type_plural' => $entity_type->getPluralLabel(),
        '%bundle' => $this->bundleInfo->getBundleInfo($entity_type_id)[$bundle]['label'],
      ];
      // These permissions are generated on behalf of $entity_display entity
      // display, therefore add this entity display as a config dependency.
      $dependencies = [
        $entity_display->getConfigDependencyKey() => [
          $entity_display->getConfigDependencyName(),
        ],
      ];
      if ($entity_type->hasKey('bundle')) {
        $permissions["use layout builder ipe on editable $bundle $entity_type_id layout overrides"] = [
          'title' => $this->t('%entity_type - %bundle: Use IPE on layout overrides for @entity_type_plural that the user can edit', $args),
          'dependencies' => $dependencies,
        ];
      }
      else {
        $permissions["use layout builder ipe on editable $entity_type_id layout overrides"] = [
          'title' => $this->t('%entity_type: Use IPE on layout overrides for @entity_type_plural that the user can edit', $args),
          'dependencies' => $dependencies,
        ];
      }
    }
    if ($this->moduleHandler->moduleExists('page_manager')) {
      $permissions['use layout builder ipe on editable page manager pages'] = [
        'title' => $this->t('Page Manager: Use IPE on page manager pages that the user can edit', $args),
      ];
    }
    return $permissions;
  }

}
