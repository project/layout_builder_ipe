<?php

namespace Drupal\layout_builder_ipe;

use Drupal\Core\Config\BootstrapConfigStorageFactory;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Modifies the layout builder tempstore repository service.
 */
class LayoutBuilderIpeServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('layout_builder.tempstore_repository') && $this->useNonConcurrentEditing()) {
      $definition = $container->getDefinition('layout_builder.tempstore_repository');
      $definition->setClass('Drupal\layout_builder_ipe\LayoutBuilderIpeTempstoreRepository')
        ->addArgument(new Reference('current_user'))
        ->addArgument(new Reference('config.factory'));
    }
  }

  /**
   * Checks whether to use non-concurrent editing.
   *
   * The default for Drupals layout builder is concurrent editing, where users
   * always see edits made by other users while configuring a layout.
   *
   * @return bool
   *   TRUE if non concurrent editing should be used, FALSE otherwise.
   */
  protected function useNonConcurrentEditing() {
    // @todo Try to swap out for config.storage to take advantage of database
    //   and caching. This might prove difficult as this is called before the
    //   container has finished building.
    // @see https://www.drupal.org/project/drupal/issues/2363351
    $config_storage = BootstrapConfigStorageFactory::get();
    $settings = $config_storage->read('layout_builder_ipe.settings');
    return !empty($settings['non_concurrent_editing']);
  }

}
