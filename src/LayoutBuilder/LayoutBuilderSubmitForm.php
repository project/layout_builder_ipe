<?php

namespace Drupal\layout_builder_ipe\LayoutBuilder;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenDialogCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder_ipe\Traits\RedirectUriTrait;
use Drupal\layout_builder_ipe\Traits\SectionStorageFormTrait;

/**
 * Service class for helping with altering the behaviour of confirm forms.
 *
 * In the context of IPE, the default behaviour of the confirmation forms for
 * "Discard changes" and "Revert to defaults" doesn't make much sense. Instead
 * of redirecting to the confirmation pages for those forms, where the cancel
 * action then redirects to the canonical URL with a deactivated IPE, we want
 * them to be displayed in a modal, that can simply be closed for canceling the
 * action, so that the users stays in context.
 */
class LayoutBuilderSubmitForm {

  use StringTranslationTrait;
  use SectionStorageFormTrait;
  use RedirectUriTrait;

  /**
   * Get the dialog options to be used for the confirm forms.
   *
   * @return array
   *   An array of dialog options.
   */
  private static function getDialogOptions() {
    return [
      'width' => 650,
      'height' => 'auto',
      'target' => 'layout-builder-modal--messages',
      'autoResize' => TRUE,
      'buttons' => [],
      'modal' => TRUE,
    ];
  }

  /**
   * Add our own handler for the given button in the LB interface.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function setSubmitFormHandler(array &$form, FormStateInterface $form_state) {
    $section_storage = self::getSectionStorageFromFormState($form_state);
    if (!$section_storage instanceof OverridesSectionStorage) {
      return;
    }
    // Ok, this is the default submit handler that simply redirects to the
    // canonical view of the entity being edited.
    $form['actions']['submit']['#ajax'] = [
      'event' => 'click',
      'callback' => [static::class, 'handleFormSubmit'],
    ];
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
  }

  /**
   * Handle a form submit that will lead to a confirmable form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An AJAX response object.
   */
  public static function handleFormSubmit(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();
    if ($form_state->hasAnyErrors()) {
      $build = [
        '#theme' => 'item_list',
        '#items' => $form_state->getErrors(),
        '#attributes' => [
          'class' => [
            'validation-errors',
          ],
        ],
      ];
      $form_state->clearErrors();
      \Drupal::messenger()->deleteAll();
      $ajax_response->addCommand(new OpenDialogCommand('#layout-builder-modal--messages', t('Error validating your changes'), $build, self::getDialogOptions()));
    }
    else {
      $section_storage = self::getSectionStorageFromFormState($form_state);
      $ajax_response->addCommand(new RedirectCommand(self::getRedirectUri($form_state) ?? $section_storage->getRedirectUrl()->toString()));
    }
    return $ajax_response;
  }

}
