<?php

namespace Drupal\layout_builder_ipe\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Validation constraint for layout builder enabled content entities.
 *
 * The idea is, that merely updating the content of an entity field should not
 * be sufficient to trigger a changed warning. Better would be if the actual
 * layout configuration can be looked at additionally to the timestamp.
 *
 * Inspired by
 * https://realize.be/blog/stop-telling-users-another-user-has-modified-content-drupal-8.
 */
class LayoutBuilderEntityChangedConstraint extends Constraint {

  /**
   * The message to show if the constraint fails.
   *
   * @var string
   */
  public $message = 'This layout has been updated @updated by another user. As a result, your changes cannot be saved.<br />You should review your changes and keep note of anything important and then use the "Discard changes" button to go back to the current version of the layout.';

}
