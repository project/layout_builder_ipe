<?php

namespace Drupal\layout_builder_ipe;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Drupal\layout_builder\LayoutTempstoreRepository;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Provide a tempstore repository for Layout Builder.
 *
 * This class basically wraps LayoutTempstoreRepository with the main
 * difference of overriding some key methods to be able to separate layout
 * sessions by user.
 */
class LayoutBuilderIpeTempstoreRepository extends LayoutTempstoreRepository {

  /**
   * The shared tempstore factory.
   *
   * @var \Drupal\Core\TempStore\SharedTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The current user.
   *
   * @var Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The layout builder ipe config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * LayoutBuilderIpeTempstoreRepository constructor.
   *
   * @param \Drupal\Core\TempStore\SharedTempStoreFactory $temp_store_factory
   *   The shared tempstore factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(SharedTempStoreFactory $temp_store_factory, AccountProxyInterface $current_user, ConfigFactoryInterface $config_factory) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->currentUser = $current_user;
    $this->config = $config_factory->get('layout_builder_ipe.settings');
  }

  /**
   * Returns the metadata associated with a particular section storage.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   *
   * @return \Drupal\Core\TempStore\Lock|null
   *   An object with the owner and updated time if the key has a value, or
   *   NULL otherwise.
   */
  public function getMetaData(SectionStorageInterface $section_storage) {
    $key = $this->getKey($section_storage);
    return $this->getTempstore($section_storage)->getMetadata($key);
  }

  /**
   * Gets the string to use as the tempstore key.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   *
   * @return string
   *   A unique string representing the section storage. This should include as
   *   much identifying information as possible about this particular storage,
   *   including information like the current language.
   */
  protected function getKey(SectionStorageInterface $section_storage) {
    $key = parent::getKey($section_storage);
    $key .= '::' . $this->currentUser->id();
    return $key;
  }

}
