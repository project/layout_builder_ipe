<?php

namespace Drupal\layout_builder_ipe\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\layout_builder_ipe\LayoutBuilderIpeService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Acces control class for IPE.
 */
class LayoutBuilderPageAccess extends ControllerBase {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The layout builder ipe service.
   *
   * @var \Drupal\layout_builder_ipe\LayoutBuilderIpeService
   */
  protected $layoutBuilderIpe;

  /**
   * Constructs a new RouteCacheContext class.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\layout_builder_ipe\LayoutBuilderIpeService $layout_builder_ipe
   *   The IPE service match.
   */
  public function __construct(RouteMatchInterface $route_match, LayoutBuilderIpeService $layout_builder_ipe) {
    $this->routeMatch = $route_match;
    $this->layoutBuilderIpe = $layout_builder_ipe;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('layout_builder_ipe')
    );
  }

  /**
   * Access callback for the "Layout" page on entities.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   An access result object.
   */
  public function accessLayoutPage() {
    $entity_form = $this->routeMatch->getRouteObject()->getDefault('_entity_form');
    if (empty($entity_form)) {
      return AccessResult::neutral();
    }
    $entity_type_id = explode('.', $entity_form)[0];

    // See if the access to the layout page should be disabled.
    if (!$this->layoutBuilderIpe->getConfig('disable_layout_page')) {
      // Returning allows to let the normal layout builder access logic take
      // effect.
      return AccessResult::allowed();
    }
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->routeMatch->getParameter($entity_type_id);
    if ($entity instanceof EntityInterface) {
      $permission = "use layout builder ipe on editable $entity_type_id layout overrides";
      if ($entity->getEntityType()->hasKey('bundle')) {
        $bundle = $entity->bundle();
        $permission = "use layout builder ipe on editable $bundle $entity_type_id layout overrides";
      }
      if (!$this->currentUser()->hasPermission($permission)) {
        // No permission to use IPE, so the layout page should be available.
        return AccessResult::allowed();
      }

      if (!$this->layoutBuilderIpe->ipeEnabled($entity)) {
        // If not enabled for this display, we allow access to the layout page.
        return AccessResult::allowed();
      }
    }

    // Prevent access.
    return AccessResult::forbidden();
  }

}
