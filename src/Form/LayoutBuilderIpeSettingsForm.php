<?php

namespace Drupal\layout_builder_ipe\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the layout builder ipe configuration form.
 */
class LayoutBuilderIpeSettingsForm extends ConfigFormBase {

  /**
   * The cache factory.
   *
   * @var \Drupal\Core\Cache\CacheFactoryInterface
   */
  protected $cacheFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->cacheFactory = $container->get('cache_factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'layout_builder_ipe_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['layout_builder_ipe.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('layout_builder_ipe.settings');
    $form['#attached']['library'] = ['layout_builder_ipe/ipe_settings'];
    $form['disable_layout_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable the "Layout" page when using IPE'),
      '#default_value' => $config->get('disable_layout_page'),
      '#description' => $this->t('Disable the "Layout" page and the corresponding local task on entity pages where Layout Builder IPE is active.'),
    ];
    $form['hide_local_tasks_block'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide the local task block while editing'),
      '#default_value' => $config->get('hide_local_tasks_block'),
      '#description' => $this->t('Hide the local task block while editing a page.'),
    ];
    $form['enhance_ui'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enhance the default Layout Builder UI'),
      '#default_value' => $config->get('enhance_ui'),
      '#description' => $this->t('Experimental: If enabled, the UI for adding new blocks to sections will be made more flexible, allowing to specifically place new blocks in between existing ones.'),
    ];
    $form['override_entity_changed_constraint'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Improve change detection on entities that use Layout Builder'),
      '#default_value' => $config->get('override_entity_changed_constraint'),
      '#description' => $this->t('Experimental: If enabled, the detection for changes to entities (one of Drupal cores default behaviors to prevent an entity save if the same entity has already been updated by another user in the meantime) will be altered. By default checks the updated date on the entity. This checkbox enables additional logic that will try to flag an entity as changed, only if actual changes to the entity have also been made. <br /><strong>Note: This might not work correctly with revisions and multilingual content, so use with care!</strong>'),
    ];
    $form['non_concurrent_editing'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Non-concurrent editing'),
      '#default_value' => $config->get('non_concurrent_editing'),
      '#description' => $this->t('Experimental: By default, changes to a nodes layout are done via shared sessions between all users who have edit access on the respective entity (concurrent editing). Enabling this setting, will create one editing session per user (non-concurrent editing). Also see <a href="@url">this Drupal core issue</a> about concurrent editing.<br /><strong>Note: Changing this modifies the layout storage and will result in data loss for any layout editing currently in process.</strong>', [
        '@url' => 'https://www.drupal.org/project/drupal/issues/3025231',
      ]),
    ];
    $form['lock_layout'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lock layout'),
      '#default_value' => $config->get('lock_layout'),
      '#description' => $this->t('Lock the layout if it is currently being edited by a user. This is only available if the <em>Non-concurrent editing</em> option is unchecked.'),
      '#states' => [
        'disabled' => [
          ':input[name="non_concurrent_editing"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['lock_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lock message'),
      '#default_value' => $config->get('lock_message'),
      '#description' => $this->t('A message shown to a user if they try to edit a locked layout This can use the following placeholders: @user, @age.<br />Leave empty to use a default message.'),
      '#states' => [
        'visible' => [
          ':input[name="non_concurrent_editing"]' => ['checked' => FALSE],
          ':input[name="lock_layout"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $original_settings = $this->config('layout_builder_ipe.settings');
    if ($original_settings->get('hide_local_tasks_block') != $form_state->getValue('hide_local_tasks_block')) {
      Cache::invalidateTags(['local_task']);
    }
    if ($original_settings->get('non_concurrent_editing') != $form_state->getValue('non_concurrent_editing')) {
      $this->cacheFactory->get('container')->deleteAll();
    }
    if ($original_settings->get('override_entity_changed_constraint') != $form_state->getValue('override_entity_changed_constraint')) {
      $this->cacheFactory->get('discovery')->delete('validation_constraint_plugins');
    }
    $this->config('layout_builder_ipe.settings')
      ->set('disable_layout_page', $form_state->getValue('disable_layout_page'))
      ->set('hide_local_tasks_block', $form_state->getValue('hide_local_tasks_block'))
      ->set('enhance_ui', $form_state->getValue('enhance_ui'))
      ->set('override_entity_changed_constraint', $form_state->getValue('override_entity_changed_constraint'))
      ->set('non_concurrent_editing', $form_state->getValue('non_concurrent_editing'))
      ->set('lock_layout', $form_state->getValue('lock_layout'))
      ->set('lock_message', $form_state->getValue('lock_message'))
      ->save();
    $this->cacheFactory->get('render')->invalidateAll();
    Cache::invalidateTags($original_settings->getCacheTags());
    parent::submitForm($form, $form_state);
  }

}
