CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended Modules
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The Layout Builder IPE module provides frontend In-Place-Editing (IPE) for
Layout Builder, similar to what Panels IPE used to do in Drupal 7.

* For a full description of the module visit
  https://www.drupal.org/project/layout_builder_ipe.

* To submit bug reports and feature suggestions, or to track changes visit
  https://www.drupal.org/project/issues/layout_builder_ipe.


REQUIREMENTS
------------

Layout Builder - Part of Drupal Core


RECOMMENDED MODULES
-------------------

Layout Builder IPE integrates with all content entities, as well as with the
Page Manager module for pages which use a Layout Builder variant type.
It also supports Gin Layout Builder and has been tested with Layout Builder
Modal.

* Gin Layout Builder - https://www.drupal.org/project/gin_lb
* Layout Builder Modal - https://www.drupal.org/project/layout_builder_modal
* Page Manager - https://www.drupal.org/project/page_manager


INSTALLATION
------------

Install the Layout Builder IPE module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420.


CONFIGURATION
-------------

1. Enable/Disable per entity type view mode.

The Layout Builder IPE can be enabled per view mode on the entity type display
page. The setting will only take effect for view modes that are used for the
full rendering of an entity on it's canonical url.

2. Permissions

Permissions must be granted to be able to see and use the IPE.

3. Configuration page

Configuration is available at /admin/config/user-interface/layout-builder-ipe:

  Administration -> Configuration -> User interface -> Layout Builder IPE

The following global options are available:
- Disable the default "Layout" page provided by the Layout Builder Module.
  The idea is to declutter the interface and prevent multiple ways of using
  Layout Builder. The default is to disable the page.
- Hide the local tasks on entity pages while editing with Layout Builder IPE.
  The default is enabled.
- Improved change detection on content entities that use Layout Builder.
  The default is disabled.
- Experimental: Non-concurrent editing. Use one editing session per user to
  keep work-in-progress config isolated. The default is disabled.
- Content locking: Only one editing session per content entity. Content locks
  can be broken by users who have the "Break locks on Layout Builder IPE"
  permission.
  The default is disabled.

Note that "Non-concurrent editing" and "Content locking" are mutually
exclusive, you can chose either one or the other, but not both.


MAINTAINERS
-----------

* berliner - https://www.drupal.org/u/berliner
