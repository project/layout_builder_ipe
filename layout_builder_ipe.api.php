<?php

/**
 * @file
 * API documentation for the Layout Builder IPE module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Alter the IPE links.
 *
 * This is typcially just a single link labeled "Customize". But modules can
 * alter it or add more links to provide custom frontend features.
 *
 * @param \Drupal\Core\Link[] $links
 *   An array of links to be rendered in the frontend IPE main action section.
 * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
 *   The section storage for the page where the links are rendered.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity of the page where the links are rendered.
 *
 * @see \Drupal\layout_builder_ipe\LayoutBuilderIpeService::attachIpe()
 */
function hook_layout_builder_ipe_links_alter(array &$links, SectionStorageInterface $section_storage, EntityInterface $entity) {
  $links['customize']->setText(t('Modify layout'));
}
