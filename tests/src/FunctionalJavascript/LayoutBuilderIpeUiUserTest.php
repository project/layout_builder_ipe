<?php

namespace Drupal\Tests\layout_builder_ipe\FunctionalJavascript;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\Tests\layout_builder\FunctionalJavascript\InlineBlockTestBase;

/**
 * Tests the Layout Builder IPE UI on non-node entities.
 *
 * @group layout_builder_ipe
 */
class LayoutBuilderIpeUiUserTest extends LayoutBuilderIpeUiBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'layout_builder_ipe',
    'node',
    'field_ui',
    'contextual',
    'block',
    'block_content',
  ];

  /**
   * Admin permissions.
   *
   * @var array
   */
  protected $adminPermissions = [
    'administer account settings',
    'administer user display',
    'administer users',
  ];

  /**
   * Editor user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $editor;

  /**
   * Editor permissions.
   *
   * @var array
   */
  protected $editorPermissions = [
    'configure editable user user layout overrides',
    'use layout builder ipe on editable user layout overrides',
    'create and edit custom blocks',
    'access contextual links',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'starterkit_theme';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a user with sufficient permissions to setup Layout Builder.
    $this->drupalLogin($this->getLbAdminUser($this->adminPermissions));

    // Enable layout builder for the first bundle.
    $this->drupalGet('admin/config/people/accounts/display');

    $page = $this->getSession()->getPage();
    $page->find('css', '[name="layout[enabled]"]')->check();
    $page->find('css', '[name="layout[allow_custom]"]')->check();
    $page->find('css', '[name="layout[layout_builder_ipe]"]')->check();
    $page->find('css', '[value="Save"]')->click();
    $this->assertSession()->pageTextContains('Your settings have been saved.');
    $this->drupalGet('admin/config/people/accounts/display');

    $this->editor = $this->drupalCreateUser($this->editorPermissions);

    $this->drupalLogout();
  }

  /**
   * Tests disabling the layout page on entities other than nodes.
   */
  public function testDisableLayoutPage() {
    $this->drupalLogin($this->editor);

    // Disable the layout page.
    $this->setIpeSetting('disable_layout_page', TRUE);

    // @todo Fix the test to work without this. Manual testing shows that this
    // works without the flush cache, see
    // layout_builder_ipe_entity_view_display_update().
    drupal_flush_all_caches();

    // Try to open the layout page.
    $this->drupalGet('user/' . $this->editor->id() . '/layout');
    $assert_session = $this->assertSession();
    $assert_session->pageTextContains('You are not authorized to access this page.');

    // Disable the setting.
    $this->setIpeSetting('disable_layout_page', FALSE);

    // Try to open the layout page again.
    $this->drupalGet('user/' . $this->editor->id() . '/layout');
    $assert_session->pageTextNotContains('You are not authorized to access this page.');
  }

  /**
   * Test the customize workflow for a non-overridden node.
   */
  public function testCustomizeWorkflow() {
    $this->drupalLogin($this->editor);

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to user view page.
    $this->drupalGet('user/' . $this->editor->id());

    // Click the customize link.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');

    // Check that the form and the three main buttons are there.
    $assert_session->elementExists('css', 'form.layout-builder-form');
    $assert_session->buttonExists('Revert to defaults');
    $assert_session->buttonExists('Discard changes');
    $assert_session->buttonExists('Save layout');

    // Save a layout without changes.
    $page->pressButton('Save layout');
    $assert_session->assertNoElementAfterWait('css', '#layout-builder-ipe-wrapper.edit-layout');
    $assert_session->pageTextContains('The layout override has been saved.');

    // Customize again to discard.
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElement('css', '#layout-builder-ipe-wrapper.edit-layout');
    $assert_session->buttonExists('Discard changes');
    $page->pressButton('Discard changes');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->responseContains('Are you sure you want to discard your layout changes?');
    $assert_session->buttonExists('Confirm');
    $page->pressButton('Confirm');
    $assert_session->assertNoElementAfterWait('css', '#layout-builder-ipe-wrapper.edit-layout');
    $assert_session->pageTextContains('The changes to the layout have been discarded.');
  }

  /**
   * Tests that custom blocks can be added.
   */
  public function testOverrideLayout() {

    // Add a new block content bundle to the editorial workflow.
    BlockContentType::create([
      'id' => 'basic',
      'label' => 'Basic',
      'revision' => 1,
    ])->save();
    block_content_add_body_field('basic');

    $this->drupalLogin($this->editor);

    // Go to user view page.
    $this->drupalGet('user/' . $this->editor->id());

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $block_title = 'The block title';
    $block_body = 'The block body';

    // Customize.
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElement('css', '#layout-builder-ipe-wrapper.edit-layout');

    // Add new block.
    $this->openAddBlockForm('Create content block');

    $textarea = $assert_session->waitForElement('css', '[name="settings[block_form][body][0][value]"]');
    $this->assertNotEmpty($textarea);
    $assert_session->fieldValueEquals('Title', '');
    $page->findField('Title')->setValue($block_title);
    $textarea->setValue($block_body);
    $page->pressButton('Add block');
    $this->assertDialogClosedAndTextVisible($block_body, InlineBlockTestBase::INLINE_BLOCK_LOCATOR);

    // Save a layout with changes.
    $page->pressButton('Save layout');
    $assert_session->assertNoElementAfterWait('css', '#layout-builder-ipe-wrapper.edit-layout');
    $assert_session->pageTextContains('The layout override has been saved.');

    // Confirm the new block is there.
    $assert_session->pageTextContains($block_title);
    $assert_session->pageTextContains($block_body);
  }

  /**
   * Tests that IPE interface is not displayed if user has no IPE permission.
   */
  public function testNoAccessNoIpePermission() {
    $this->drupalLogin($this->createUser([
      'configure editable user user layout overrides',
      'create and edit custom blocks',
      'access contextual links',
    ]));

    // Go to user view page.
    $this->drupalGet('user/' . $this->editor->id());

    // Click the customize link.
    $assert_session = $this->assertSession();
    $assert_session->elementNotExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementNotExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkNotExists('Customize');
  }

  /**
   * Tests that IPE interface is not displayed if disabled for the view mode.
   */
  public function testNoAccessViewModeDisabled() {
    $this->drupalLogin($this->getLbAdminUser());

    // Enable layout builder.
    $this->drupalGet('admin/config/people/accounts/display');

    $page = $this->getSession()->getPage();
    $page->find('css', '[name="layout[enabled]"]')->check();
    $page->find('css', '[name="layout[allow_custom]"]')->check();
    $page->find('css', '[name="layout[layout_builder_ipe]"]')->uncheck();
    $page->find('css', '[value="Save"]')->click();

    $this->drupalLogin($this->editor);

    // Go to user view page.
    $this->drupalGet('user/' . $this->editor->id());

    // Click the customize link.
    $assert_session = $this->assertSession();
    $assert_session->elementNotExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementNotExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkNotExists('Customize');
  }

  /**
   * Tests that IPE interface is not displayed if user has no edit permission.
   */
  public function testNoAccessNoEditPermission() {
    $this->drupalLogin($this->editor);

    $other_user = $this->createUser();

    // Go to user view page.
    $this->drupalGet('user/' . $other_user->id());

    // Click the customize link.
    $assert_session = $this->assertSession();
    $assert_session->elementNotExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementNotExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkNotExists('Customize');
  }

}
