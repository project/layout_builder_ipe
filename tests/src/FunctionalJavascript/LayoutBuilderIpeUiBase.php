<?php

namespace Drupal\Tests\layout_builder_ipe\FunctionalJavascript;

use Drupal\Core\Session\AccountInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\contextual\FunctionalJavascript\ContextualLinkClickTrait;
use Drupal\Tests\system\Traits\OffCanvasTestTrait;

/**
 * Tests the Layout Builder IPE UI.
 *
 * @group layout_builder_ipe
 */
abstract class LayoutBuilderIpeUiBase extends WebDriverTestBase {

  use ContextualLinkClickTrait;
  use OffCanvasTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'starterkit_theme';

  /**
   * Admin permissions.
   *
   * @var array
   */
  protected $adminPermissions = [];

  /**
   * Set a global IPE setting.
   *
   * @param string $settings_key
   *   The key of the setting.
   * @param bool $state
   *   The state to set for the setting.
   */
  protected function setIpeSetting($settings_key, $state) {
    $logged_in_user = $this->loggedInUser;
    $this->drupalLogin($this->getLbAdminUser());

    // Confirm that the setting to disable the layout page is checked.
    $this->drupalGet('admin/config/user-interface/layout-builder-ipe');
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();
    if ($state) {
      $page->find('css', '[name="' . $settings_key . '"]')->check();
    }
    else {
      $page->find('css', '[name="' . $settings_key . '"]')->uncheck();
    }
    $page->find('css', '[value="Save configuration"]')->click();
    $assert_session->responseContains('The configuration options have been saved.');

    if ($state) {
      $assert_session->checkboxChecked($settings_key);
    }
    else {
      $assert_session->checkboxNotChecked($settings_key);
    }
    // And login the original user.
    if ($logged_in_user) {
      $this->drupalLogin($logged_in_user);
    }
    else {
      $this->drupalLogout();
    }
  }

  /**
   * Get a user object with a full set of Layout Builder permissions.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   A user account object.
   */
  protected function getLbAdminUser($permissions = []) {
    $permissions = array_merge($permissions, $this->adminPermissions);
    sort($permissions);
    $hash = !empty($permissions) ? md5(implode(',', $permissions)) : 'default';
    static $users = [];
    if (empty($users[$hash])) {
      $users[$hash] = $this->drupalCreateUser(array_merge([
        'configure any layout',
        'create and edit custom blocks',
        'administer layout builder ipe',
        'access contextual links',
      ], $permissions));
    }
    return $users[$hash];
  }

  /**
   * Opens the add block form in the off-canvas dialog.
   *
   * @param string $block_title
   *   The block title which will be the link text.
   */
  protected function openAddBlockForm($block_title) {
    $assert_session = $this->assertSession();
    $assert_session->linkExists('Add block');
    $this->clickLink('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $this->assertNotEmpty($assert_session->waitForElementVisible('named', [
      'link',
      $block_title,
    ]));
    $this->clickLink($block_title);
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#drupal-off-canvas');
  }

  /**
   * Asserts that the dialog closes and the new text appears on the main canvas.
   *
   * @param string $text
   *   The text.
   * @param string|null $css_locator
   *   The css locator to use inside the main canvas if any.
   */
  protected function assertDialogClosedAndTextVisible($text, $css_locator = NULL) {
    $assert_session = $this->assertSession();
    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->elementNotExists('css', '#drupal-off-canvas');
    if ($css_locator) {
      $this->assertNotEmpty($assert_session->waitForElementVisible('css', ".dialog-off-canvas-main-canvas $css_locator:contains('$text')"));
    }
    else {
      $this->assertNotEmpty($assert_session->waitForElementVisible('css', ".dialog-off-canvas-main-canvas:contains('$text')"));
    }
  }

}
