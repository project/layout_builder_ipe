<?php

namespace Drupal\Tests\layout_builder_ipe\FunctionalJavascript;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\Tests\layout_builder\FunctionalJavascript\InlineBlockTestBase;

/**
 * Tests the Layout Builder IPE UI on node entities.
 *
 * @group layout_builder_ipe
 */
class LayoutBuilderIpeUiNodeTest extends LayoutBuilderIpeUiBase {

  const BUNDLE_LB = 'bundle_layout_builder';
  const BUNDLE_NO_LB = 'bundle_no_layout_builder';

  /**
   * Path prefix for the field UI for the test bundle.
   *
   * @var string
   */
  const FIELD_UI_PREFIX_LB = 'admin/structure/types/manage/' . self::BUNDLE_LB;

  /**
   * Path prefix for the field UI for the test bundle.
   *
   * @var string
   */
  const FIELD_UI_PREFIX_NO_LB = 'admin/structure/types/manage/' . self::BUNDLE_NO_LB;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'layout_builder_ipe',
    'node',
    'field_ui',
    'contextual',
    'block',
    'block_content',
  ];

  /**
   * Node using Layout Builder.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $nodeLb;

  /**
   * Node not using Layout Builder.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $nodeNoLb;

  /**
   * Admin permissions.
   *
   * @var array
   */
  protected $adminPermissions = [
    'administer node display',
    'administer node fields',
    'access contextual links',
  ];

  /**
   * Editor user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $editor;

  /**
   * Editor permissions.
   *
   * @var array
   */
  protected $editorPermissions = [
    'edit any ' . self::BUNDLE_LB . ' content',
    'configure editable ' . self::BUNDLE_LB . ' node layout overrides',
    'use layout builder ipe on editable ' . self::BUNDLE_LB . ' node layout overrides',
    'create and edit custom blocks',
    'access contextual links',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'starterkit_theme';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a user with sufficient permissions to setup Layout Builder.
    $this->drupalLogin($this->getLbAdminUser());

    // Create content types.
    $this->createContentType(['type' => self::BUNDLE_LB]);
    $this->createContentType(['type' => self::BUNDLE_NO_LB]);

    // Enable layout builder for the first bundle.
    $this->drupalGet(static::FIELD_UI_PREFIX_LB . '/display/default');

    $page = $this->getSession()->getPage();
    $page->find('css', '[name="layout[enabled]"]')->check();
    $page->find('css', '[name="layout[allow_custom]"]')->check();
    $page->find('css', '[name="layout[layout_builder_ipe]"]')->check();
    $page->find('css', '[value="Save"]')->click();
    $this->assertSession()->pageTextContains('Your settings have been saved.');
    $this->drupalGet(static::FIELD_UI_PREFIX_LB . '/display/default');

    // Disable layout builder for the second bundle.
    $this->drupalGet(static::FIELD_UI_PREFIX_NO_LB . '/display/default');

    $page = $this->getSession()->getPage();
    $page->find('css', '[name="layout[enabled]"]')->uncheck();
    $page->find('css', '[name="layout[allow_custom]"]')->uncheck();
    $page->find('css', '[name="layout[layout_builder_ipe]"]')->uncheck();
    $page->find('css', '[value="Save"]')->click();
    $this->assertSession()->pageTextContains('Your settings have been saved.');
    $this->drupalGet(static::FIELD_UI_PREFIX_NO_LB . '/display/default');

    // Create a node for each bundle.
    $this->nodeLb = $this->createNode([
      'type' => self::BUNDLE_LB,
      'title' => "Node LB title (nid 1)",
    ]);
    $this->nodeNoLb = $this->createNode([
      'type' => self::BUNDLE_NO_LB,
      'title' => "Node NO LB title (nid 2)",
    ]);

    $this->editor = $this->drupalCreateUser($this->editorPermissions);

    $this->drupalLogout();
  }

  /**
   * Tests disabling the layout page on node entities.
   */
  public function testDisableLayoutPage() {
    $this->drupalLogin($this->editor);

    // Disable the layout page.
    $this->setIpeSetting('disable_layout_page', TRUE);

    // Try to open the layout page.
    $this->drupalGet($this->nodeLb->toUrl()->toString() . '/layout');
    $assert_session = $this->assertSession();
    $assert_session->pageTextContains('You are not authorized to access this page.');

    // Disable the setting.
    $this->setIpeSetting('disable_layout_page', FALSE);

    // Try to open the layout page again.
    $this->drupalGet($this->nodeLb->toUrl()->toString() . '/layout');
    $assert_session->pageTextNotContains('You are not authorized to access this page.');
  }

  /**
   * Test the customize workflow for a non-overridden node.
   */
  public function testCustomizeWorkflow() {

    $this->drupalLogin($this->editor);

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');

    // Check that the form and the three main buttons are there.
    $assert_session->elementExists('css', 'form.layout-builder-form');
    $assert_session->buttonExists('Revert to defaults');
    $assert_session->buttonExists('Discard changes');
    $assert_session->buttonExists('Save layout');

    // Save a layout without changes.
    $page->pressButton('Save layout');
    $assert_session->assertNoElementAfterWait('css', '#layout-builder-ipe-wrapper.edit-layout');
    $assert_session->pageTextContains('The layout override has been saved.');

    // Customize again to discard.
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElement('css', '#layout-builder-ipe-wrapper.edit-layout');
    $assert_session->buttonExists('Discard changes');
    $page->pressButton('Discard changes');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->responseContains('Are you sure you want to discard your layout changes?');
    $assert_session->buttonExists('Confirm');
    $page->pressButton('Confirm');
    $assert_session->assertNoElementAfterWait('css', '#layout-builder-ipe-wrapper.edit-layout');
    $assert_session->pageTextContains('The changes to the layout have been discarded.');
  }

  /**
   * Tests that custom blocks can be added.
   */
  public function testOverrideLayout() {

    // Add a new block content bundle to the editorial workflow.
    BlockContentType::create([
      'id' => 'basic',
      'label' => 'Basic',
      'revision' => 1,
    ])->save();
    block_content_add_body_field('basic');

    $this->drupalLogin($this->editor);

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $block_title = 'The block title';
    $block_body = 'The block body';

    // Customize.
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElement('css', '#layout-builder-ipe-wrapper.edit-layout');

    // Add new block.
    $this->openAddBlockForm('Create content block');

    $textarea = $assert_session->waitForElement('css', '[name="settings[block_form][body][0][value]"]');
    $this->assertNotEmpty($textarea);
    $assert_session->fieldValueEquals('Title', '');
    $page->findField('Title')->setValue($block_title);
    $textarea->setValue($block_body);
    $page->pressButton('Add block');
    $this->assertDialogClosedAndTextVisible($block_body, InlineBlockTestBase::INLINE_BLOCK_LOCATOR);

    // Save a layout with changes.
    $page->pressButton('Save layout');
    $assert_session->assertNoElementAfterWait('css', '#layout-builder-ipe-wrapper.edit-layout');
    $assert_session->pageTextContains('The layout override has been saved.');

    // Confirm the new block is there.
    $assert_session->pageTextContains($block_title);
    $assert_session->pageTextContains($block_body);
  }

  /**
   * Tests that IPE interface is not displayed if user has no IPE permission.
   */
  public function testNoAccessNoIpePermission() {
    $this->drupalLogin($this->drupalCreateUser([
      'edit any ' . self::BUNDLE_LB . ' content',
      'configure editable ' . self::BUNDLE_LB . ' node layout overrides',
      'create and edit custom blocks',
      'access contextual links',
    ]));

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link.
    $assert_session = $this->assertSession();
    $assert_session->elementNotExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementNotExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkNotExists('Customize');
  }

  /**
   * Tests that IPE interface is not displayed if disabled for the view mode.
   */
  public function testNoAccessViewModeDisabled() {
    $this->drupalLogin($this->getLbAdminUser());

    // Enable layout builder.
    $this->drupalGet(static::FIELD_UI_PREFIX_LB . '/display/default');

    $page = $this->getSession()->getPage();
    $page->find('css', '[name="layout[enabled]"]')->check();
    $page->find('css', '[name="layout[allow_custom]"]')->check();
    $page->find('css', '[name="layout[layout_builder_ipe]"]')->uncheck();
    $page->find('css', '[value="Save"]')->click();

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link.
    $assert_session = $this->assertSession();
    $assert_session->elementNotExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementNotExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkNotExists('Customize');
  }

  /**
   * Tests that IPE interface is not displayed if user has no edit permission.
   */
  public function testNoAccessNoEditPermission() {
    $this->drupalLogin($this->drupalCreateUser([
      'configure editable ' . self::BUNDLE_LB . ' node layout overrides',
      'use layout builder ipe on editable ' . self::BUNDLE_LB . ' node layout overrides',
      'create and edit custom blocks',
      'access contextual links',
    ]));

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link.
    $assert_session = $this->assertSession();
    $assert_session->elementNotExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementNotExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkNotExists('Customize');
  }

  /**
   * Tests that IPE interface is not displayed if the node doesn't use LB.
   */
  public function testNoAccessNoLayoutBuilder() {
    $this->drupalLogin($this->drupalCreateUser([
      'edit any ' . self::BUNDLE_NO_LB . ' content',
      'create and edit custom blocks',
      'access contextual links',
    ]));

    // Go to node view page.
    $this->drupalGet($this->nodeNoLb->toUrl()->toString());

    // Click the customize link.
    $assert_session = $this->assertSession();
    $assert_session->elementNotExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementNotExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkNotExists('Customize');
  }

  /**
   * Tests that IPE correctly shows error messages for concurrent edits.
   */
  public function testConcurrentEdits() {
    $this->drupalLogin($this->editor);

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');

    // Now, without discarding or saving, navigate to the edit form and save
    // that.
    $this->drupalGet($this->nodeLb->toUrl('edit-form')->toString());
    $page->pressButton('Save');

    // Customize again and try to save. We should see an error that the page
    // can't be saved due to modifications done in the meantime.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');

    // Check that the form and the three main buttons are there.
    $assert_session->elementExists('css', 'form.layout-builder-form');
    $assert_session->buttonExists('Revert to defaults');
    $assert_session->buttonExists('Discard changes');
    $assert_session->buttonExists('Save layout');

    // Save a layout without changes.
    $page->pressButton('Save layout');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->responseContains('The content has either been modified by another user, or you have already submitted modifications. As a result, your changes cannot be saved.');
  }

  /**
   * Tests that IPE correctly shows error messages for concurrent edits.
   */
  public function testConcurrentEditsEnhanced() {
    // Enable enhanced entity changed checking.
    $this->setIpeSetting('override_entity_changed_constraint', TRUE);

    $this->drupalLogin($this->editor);
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link to open an edit session.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');

    // Now, without discarding or saving, navigate to the edit form and save
    // that.
    $this->drupalGet($this->nodeLb->toUrl('edit-form')->toString());
    $page->pressButton('Save');

    // Customize again and try to save. This should work.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');

    // Check that the form and the three main buttons are there.
    $assert_session->elementExists('css', 'form.layout-builder-form');
    $assert_session->buttonExists('Revert to defaults');
    $assert_session->buttonExists('Discard changes');
    $assert_session->buttonExists('Save layout');

    // Save a layout without changes.
    $page->pressButton('Save layout');
    $assert_session->waitForText('The layout override has been saved.');
  }

  /**
   * Tests that IPE uses shared layout sessions per user.
   */
  public function testSharedLayoutSessionPerUser() {
    // Enable enhanced entity changed checking.
    $this->setIpeSetting('non_concurrent_editing', FALSE);

    $this->drupalLogin($this->editor);
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link to open an edit session.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');
    $assert_session->pageTextNotContains('Powered by Drupal');

    // Add a new block.
    $this->openAddBlockForm('Powered by Drupal');

    $page->fillField('settings[label]', 'This is the label');
    $page->checkField('settings[label_display]');

    // Save the new block, and ensure it is displayed on the page.
    $page->pressButton('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->pageTextContains('Powered by Drupal');
    $assert_session->pageTextContains('This is the label');

    // Now login as a new user with the same permissions.
    $this->drupalLogin($this->drupalCreateUser($this->editorPermissions));

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link to open an edit session.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');

    // And confirm that the changes made by the previous user are visible
    // to this one.
    $assert_session->pageTextContains('Powered by Drupal');
    $assert_session->pageTextContains('This is the label');
  }

  /**
   * Tests that IPE uses separate layout sessions per user.
   */
  public function testSeparateLayoutSessionPerUser() {
    // Enable enhanced entity changed checking.
    $this->setIpeSetting('non_concurrent_editing', TRUE);

    $this->drupalLogin($this->drupalCreateUser($this->editorPermissions));
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link to open an edit session.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');
    $assert_session->pageTextNotContains('Powered by Drupal');

    // Add a new block.
    $this->openAddBlockForm('Powered by Drupal');

    $page->fillField('settings[label]', 'This is the label');
    $page->checkField('settings[label_display]');

    // Save the new block, and ensure it is displayed on the page.
    $page->pressButton('Add block');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->assertNoElementAfterWait('css', '#drupal-off-canvas');
    $assert_session->pageTextContains('Powered by Drupal');
    $assert_session->pageTextContains('This is the label');

    // Now login as a new user with the same permissions.
    $this->drupalLogin($this->drupalCreateUser($this->editorPermissions));

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link to open an edit session.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');

    // And confirm that the changes made by the previous user are not visible
    // to this one.
    $assert_session->pageTextNotContains('Powered by Drupal');
    $assert_session->pageTextNotContains('This is the label');
  }

  /**
   * Tests that IPE uses separate layout sessions per user.
   */
  public function testLayoutLock() {
    // Enable locking.
    $this->setIpeSetting('non_concurrent_editing', FALSE);
    $this->setIpeSetting('lock_layout', TRUE);

    $user_1 = $this->drupalCreateUser($this->editorPermissions);
    $this->drupalLogin($user_1);

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link to open an edit session.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');
    // Check that the form and the three main buttons are there.
    $assert_session->elementExists('css', 'form.layout-builder-form');
    $assert_session->buttonExists('Revert to defaults');
    $assert_session->buttonExists('Discard changes');
    $assert_session->buttonExists('Save layout');

    // We need to save at least once to create an override section storage.
    $page = $this->getSession()->getPage();
    $page->pressButton('Save layout');
    $assert_session->assertWaitOnAjaxRequest();

    // Now click customize again to lock this layout.
    $page = $this->getSession()->getPage();
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');

    // Now login as a new user with the same permissions.
    $this->drupalLogin($this->drupalCreateUser($this->editorPermissions));

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link to open an edit session.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#drupal-modal');
    $assert_session->pageTextContains('This page is currently being edited by ' . $user_1->getDisplayName() . ' and is therefore locked from editing by others');
    $assert_session->buttonExists('Ok');
    $assert_session->buttonNotExists('Break lock');
  }

  /**
   * Tests that IPE uses separate layout sessions per user.
   */
  public function testLayoutLockWithBreak() {
    // Enable locking.
    $this->setIpeSetting('non_concurrent_editing', FALSE);
    $this->setIpeSetting('lock_layout', TRUE);

    $permissions = array_merge($this->editorPermissions, [
      'layout builder ipe break locks',
    ]);
    $user_1 = $this->drupalCreateUser($permissions);
    $this->drupalLogin($user_1);

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link to open an edit session.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');
    // Check that the form and the three main buttons are there.
    $assert_session->elementExists('css', 'form.layout-builder-form');
    $assert_session->buttonExists('Revert to defaults');
    $assert_session->buttonExists('Discard changes');
    $assert_session->buttonExists('Save layout');

    // We need to save at least once to create an override section storage.
    $page = $this->getSession()->getPage();
    $page->pressButton('Save layout');
    $assert_session->assertWaitOnAjaxRequest();

    // Now click customize again to lock this layout.
    $page = $this->getSession()->getPage();
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '#layout-builder-ipe-wrapper.edit-layout');
    // Check that the form and the three main buttons are there.
    $assert_session->elementExists('css', 'form.layout-builder-form');
    $assert_session->buttonExists('Revert to defaults');
    $assert_session->buttonExists('Discard changes');
    $assert_session->buttonExists('Save layout');

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Now login as a new user with the same permissions.
    $this->drupalLogin($this->drupalCreateUser($permissions));

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Go to node view page.
    $this->drupalGet($this->nodeLb->toUrl()->toString());

    // Click the customize link to open an edit session.
    $assert_session->elementExists('css', '#layout-builder-ipe-wrapper');
    $assert_session->elementExists('css', '.layout-builder-ipe-actions');
    $assert_session->linkExists('Customize');
    $page->clickLink('Customize');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->waitForElementVisible('css', '[role="dialog"] #drupal-modal');
    $assert_session->elementExists('css', '[role="dialog"] #drupal-modal');
    $assert_session->pageTextContains('This page is currently being edited by ' . $user_1->getDisplayName() . ' and is therefore locked from editing by others');
    $assert_session->pageTextContains('You can break the look to start editing this page');
    $assert_session->buttonExists('Ok');
    $assert_session->buttonExists('Break lock');

    // Now break the lock and confirm that it's working.
    $page->pressButton('Break lock');
    $assert_session->assertWaitOnAjaxRequest();
    $assert_session->assertNoElementAfterWait('css', '.ui-dialog.locked-message');
    // Check that the form and the three main buttons are there.
    $assert_session->elementExists('css', 'form.layout-builder-form');
    $assert_session->buttonExists('Revert to defaults');
    $assert_session->buttonExists('Discard changes');
    $assert_session->buttonExists('Save layout');
  }

}
