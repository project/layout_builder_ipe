<?php

namespace Drupal\Tests\layout_builder_ipe\FunctionalJavascript;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests core regressions introduced by Layout Builder IPE.
 *
 * @group layout_builder_ipe
 */
class LayoutBuilderIntegrationTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'layout_builder_ipe',
    'node',
    'field_ui',
    'contextual',
  ];

  const BUNDLE_LB = 'bundle_layout_builder';
  const BUNDLE_NO_LB = 'bundle_no_layout_builder';

  /**
   * Path prefix for the field UI for the test bundle.
   *
   * @var string
   */
  const FIELD_UI_PREFIX_LB = 'admin/structure/types/manage/' . self::BUNDLE_LB;

  /**
   * Path prefix for the field UI for the test bundle.
   *
   * @var string
   */
  const FIELD_UI_PREFIX_NO_LB = 'admin/structure/types/manage/' . self::BUNDLE_NO_LB;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a user with sufficient permissions to setup Layout Builder.
    $this->drupalLogin($this->drupalCreateUser([
      'configure any layout',
      'create and edit custom blocks',
      'administer node display',
      'administer node fields',
      'access contextual links',
      'administer layout builder ipe',
    ]));

    // Create content types.
    $this->createContentType(['type' => self::BUNDLE_LB]);
    $this->createContentType(['type' => self::BUNDLE_NO_LB]);

    // Enable layout builder for the first bundle.
    $this->drupalGet(static::FIELD_UI_PREFIX_LB . '/display/default');

    $page = $this->getSession()->getPage();
    $page->find('css', '[name="layout[enabled]"]')->check();
    $page->find('css', '[name="layout[allow_custom]"]')->check();
    $page->find('css', '[name="layout[layout_builder_ipe]"]')->check();
    $page->find('css', '[value="Save"]')->click();
    $this->assertSession()->pageTextContains('Your settings have been saved.');
    $this->drupalGet(static::FIELD_UI_PREFIX_LB . '/display/default');

    // Disable layout builder for the second bundle.
    $this->drupalGet(static::FIELD_UI_PREFIX_NO_LB . '/display/default');

    $page = $this->getSession()->getPage();
    $page->find('css', '[name="layout[enabled]"]')->uncheck();
    $page->find('css', '[name="layout[allow_custom]"]')->uncheck();
    $page->find('css', '[name="layout[layout_builder_ipe]"]')->uncheck();
    $page->find('css', '[value="Save"]')->click();
    $this->assertSession()->pageTextContains('Your settings have been saved.');
    $this->drupalGet(static::FIELD_UI_PREFIX_NO_LB . '/display/default');

    $this->drupalLogout();
  }

  /**
   * Test creation of new node.
   *
   * Tests that there is no exception when trying to add a new node that has
   * IPE turned on.
   */
  public function testCreateNewNode() {
    $this->drupalLogin($this->drupalCreateUser([
      'create ' . self::BUNDLE_LB . ' content',
    ]));

    $this->drupalGet('node/add/' . self::BUNDLE_LB);

    $assert_session = $this->assertSession();
    $assert_session->buttonExists('Save');
  }

}
