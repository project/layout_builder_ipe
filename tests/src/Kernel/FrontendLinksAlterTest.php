<?php

namespace Drupal\Tests\layout_builder_ipe\Kernel;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Markup;
use Drupal\KernelTests\KernelTestBase;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder_ipe\LayoutBuilderIpeService;

/**
 * Tests the effectiveness of hook_system_info_alter().
 *
 * @group system
 */
class FrontendLinksAlterTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'frontend_links_test',
  ];

  /**
   * Tests that theme .info.yml data is rebuild after enabling a module.
   *
   * Tests that info data is rebuilt after a module that implements
   * hook_system_info_alter() is enabled. Also tests if core *_list() functions
   * return freshly altered info.
   */
  public function testFrontendLinksAlter() {
    // Mock objects.
    $section_storage = $this->getMockBuilder(SectionStorageInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $entity = $this->getMockBuilder(EntityInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $layout_builder_ipe = $this->getMockBuilder(LayoutBuilderIpeService::class)
      ->disableOriginalConstructor()
      ->getMock();

    // Inject the necessary services into $layout_builder_ipe and make the
    // private method renderFrontendLinks accessible for the test.
    $reflection = new \ReflectionClass(get_class($layout_builder_ipe));
    $method = $reflection->getMethod('renderFrontendLinks');
    $method->setAccessible(TRUE);
    $property = $reflection->getProperty('moduleHandler');
    $property->setAccessible(TRUE);
    $property->setValue($layout_builder_ipe, $this->container->get('module_handler'));
    $property = $reflection->getProperty('renderer');
    $property->setAccessible(TRUE);
    $property->setValue($layout_builder_ipe, $this->container->get('renderer'));

    $links = [
      'customize' => Markup::create('Customize button'),
    ];
    $build = [];

    // Let the service build the frontend links.
    $links = $method->invokeArgs($layout_builder_ipe, [
      $links,
      $section_storage,
      $entity,
      &$build,
    ]);

    // Confirm all expected links are still there.
    $this->assertArrayHasKey('customize', $links, 'Links contain the original link');
    $this->assertArrayHasKey('renderable', $links, 'Links contain renderable');
    $this->assertArrayHasKey('render_array', $links, 'Links contain render array');
    $this->assertArrayHasKey('markup', $links, 'Links contain markup');
    $this->assertArrayHasKey('string', $links, 'Links contain string');

    // Confirm all links have been rendered.
    $this->assertTrue(is_string($links['customize']), 'Original link has been rendered.');
    $this->assertTrue(is_string($links['renderable']), 'Renderable object has been rendered.');
    $this->assertTrue(is_string($links['render_array']), 'Render array has been rendered.');
    $this->assertTrue(is_string($links['markup']), 'Markup object has been rendered.');
    $this->assertTrue(is_string($links['string']), 'String is still there.');
  }

}
